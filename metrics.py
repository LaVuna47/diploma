from sklearn.metrics import mean_absolute_error
from numpy import sqrt
from sklearn.metrics import mean_squared_error, r2_score, mean_absolute_percentage_error, max_error, \
    median_absolute_error, mean_absolute_error


def print_metrics(y_true, y_pred):
    mse = mean_squared_error(y_true, y_pred)
    print(f'Mean Squared Error (MSE):              {mse:.10f}')

    rmse = sqrt(mse)
    print(f'Root Mean Squared Error (RMSE):        {rmse:.10f}')

    mae = mean_absolute_error(y_true, y_pred)
    print(f'Mean Absolute Error (MAE):             {mae:.10f}')

    r2 = r2_score(y_true, y_pred)
    print(f'R-squared (R²):                        {r2:.10f}')

    mape = mean_absolute_percentage_error(y_true, y_pred)
    print(f'Mean Absolute Percentage Error (MAPE): {mape:.10f}')

    me = max_error(y_true, y_pred)
    print(f'Max Error (ME):                        {me:.10f}')

    med_ae = median_absolute_error(y_true, y_pred)
    print(f'Median Absolute Error (MedAE):         {med_ae:.10f}')


def get_metrics(y_true, y_pred):
    mse = mean_squared_error(y_true, y_pred)
    rmse = sqrt(mse)
    mae = mean_absolute_error(y_true, y_pred)
    r2 = r2_score(y_true, y_pred)
    mape = mean_absolute_percentage_error(y_true, y_pred)
    me = max_error(y_true, y_pred)
    med_ae = median_absolute_error(y_true, y_pred)
    return [mse, rmse, mae, r2, mape, me, med_ae]



def get_averaged_metrics(metrics):
    assert len(metrics) > 0
    res = [0 for _ in range(len(metrics[0]))]
    for i in range(len(metrics)):
        for j in range(len(metrics[i])):
            res[j] += metrics[i][j]
    for i in range(len(metrics[0])):
        res[i] /= len(metrics)
    return res

def print_averaged_metrics(metrics):
    print(f'Mean Squared Error (MSE):              {metrics[0]:.10f}')
    print(f'Root Mean Squared Error (RMSE):        {metrics[1]:.10f}')
    print(f'Mean Absolute Error (MAE):             {metrics[2]:.10f}')
    print(f'R-squared (R²):                        {metrics[3]:.10f}')
    print(f'Mean Absolute Percentage Error (MAPE): {metrics[4]:.10f}')
    print(f'Max Error (ME):                        {metrics[5]:.10f}')
    print(f'Median Absolute Error (MedAE):         {metrics[6]:.10f}')

def write_metrics_to_file(metrics, filename):
    with open(filename, 'a') as file:
        file.write(f'{metrics[0]:.5f}, ')
        file.write(f'{metrics[1]:.5f}, ')
        file.write(f'{metrics[2]:.5f}, ')
        file.write(f'{metrics[3]:.5f}, ')
        file.write(f'{metrics[4]:.5f}, ')
        file.write(f'{metrics[5]:.5f}, ')
        file.write(f'{metrics[6]:.5f} ')
        file.write(f'\n')

def write_to_file(y_true, y_pred, file_name):
    with open(file_name, 'a') as file:
        mse = mean_squared_error(y_true, y_pred)
        file.write(f'{mse:.5f}, ')

        rmse = sqrt(mse)
        file.write(f'{rmse:.5f}, ')

        mae = mean_absolute_error(y_true, y_pred)
        file.write(f'{mae:.5f}, ')

        r2 = r2_score(y_true, y_pred)
        file.write(f'{r2:.5f}, ')

        mape = mean_absolute_percentage_error(y_true, y_pred)
        file.write(f'{mape:.5f}, ')

        me = max_error(y_true, y_pred)
        file.write(f'{me:.5f}, ')

        med_ae = median_absolute_error(y_true, y_pred)
        file.write(f'{med_ae:.5f}')
        file.write(f'\n')