#!/bin/bash

# Check if the correct number of arguments is provided
if [ "$#" -ne 2 ]; then
  echo "Usage: $0 <directory> <string>"
  exit 1
fi

# Assign arguments to variables
directory=$1
string=$2

# Find and print the contents of files ending with the specific string
for file in "$directory"/*"$string"; do
  if [ -f "$file" ]; then
    filename=$(basename "$file")
    base_filename="${filename%$string}"
    echo -n "$base_filename,"
    cat "$file"
  fi
done

echo -e "\n"